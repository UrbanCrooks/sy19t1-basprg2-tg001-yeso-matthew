#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#ifndef NODE_H
#define NODE_H

using namespace std;

struct Node
{
	string name;
	Node* next = NULL;
	Node* previous = NULL;
};
#endif

Node* createList(int& soldierCount)
{
	int counter = 0;
	string soldierName;
	Node* head = NULL;
	Node* tail = NULL;


	do {
		cout << "What are you names? ";
		cin >> soldierName;
		Node* soldier = new Node;
		soldier->name = soldierName;
		if (head == NULL) //First in the list
		{
			head = soldier;
			tail = soldier;
		}
		else //Next in the list
		{
			tail->next = soldier;
			soldier->previous = tail;
			tail = soldier;
		}
		counter++;
	} while (counter != soldierCount); //Input names until soldier count is reached
	tail->next = head;
	head->previous = tail; //Connect tail to head

	for (int i = 0; i < rand() % soldierCount; i++) //Randomized Starting Point
	{
		head = head->next;
	}

	return head;
}



Node* passCloak(int& soldierNum, Node* head)
{
	Node*holder = head;
	int randomRoll = rand() % soldierNum + 1;
	cout << "Drawn " << randomRoll << endl;

	for (int i = 0; i < randomRoll; i++) //Passes cloak until the loop ends
	{
		holder = holder->next;
	}
	cout << "The Holder Of The Cloak Is " << holder->name << endl;
	head = holder;

	return head;
}

Node* removeSoldier(int& numOfSoldiers, Node* head)
{
	Node*remove = head;

	remove->previous->next = remove->next; //Set next node to be in the position of the current one
	remove->next->previous = remove->previous; // Connect the previous node to the new current one

	head = remove->next; //new head
	delete remove;

	numOfSoldiers -= 1;
	cout << numOfSoldiers << " Left" << endl;
	return head;
}

Node* displayResults(int& numOfSoldiers, Node* head)
{
	Node*temp = head;
	cout << endl;
	cout << "Soldiers left: " << endl;
	for (int i = 0; i < numOfSoldiers; i++)
	{
		cout << temp->name << endl;
		temp = temp->next;
	}
	return head;
}


int main()
{
	srand(time(NULL));
	int soldierAmount = 0;
	int roundCount = 0;
	Node*head = new Node;


	cout << "How many soldiers are willing to paritcipate? ";
	cin >> soldierAmount;
	cout << endl;
	head = createList(soldierAmount);
	displayResults(soldierAmount, head);

	do
	{
		roundCount++;
		cout << "====================" << endl;
		cout << "      Round " << roundCount << endl;
		cout << "====================" << endl;
		cout << endl;
		head = passCloak(soldierAmount, head);
		head = removeSoldier(soldierAmount, head);
		displayResults(soldierAmount, head);
		cout << endl;
	} while (soldierAmount > 1);

	system("pause");
	return 0;
}