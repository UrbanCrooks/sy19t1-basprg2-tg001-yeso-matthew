#include <iostream>
#include <string>

using namespace std;

int factorial(int n)
{
	int result = 1;

	if (n < 0)
	{
		cout << "Invalid Parameter " << endl;
		return 0;
	}
	else if (n == 0)
	{
		result = 1;
	}

	for (int i = 1; i <= n; i++)
	{
		result *= i;
	}
	return result;
}
int main()
{
	int input;
	cout << "Enter Number: ";
	cin >> input;
	int sequence = factorial(input);
	cout << "factorial: " << sequence << endl;

	system("pause");
	return 0;

}