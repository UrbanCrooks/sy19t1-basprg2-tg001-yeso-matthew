#include "Unit.h"
#include<iostream>
#include<string>
#include<vector>
#include<time.h>
using namespace std;



Unit::Unit()//Constructor sets stats 
{
	srand(time(NULL));
	maxHP = rand() % 15 + 15;
	hp = maxHP;
	power = rand() % 10 + 1;
	vitality = rand() % 10 + 1;
	agility = rand() % 10 + 1;
	dexterity = rand() % 10 + 1;
	canFight = true;
	bonusDmg = 1.0;
}


Unit::~Unit()
{
}

void Unit::initializeClass()//Selects class
{
	if (this->classType == 1)
	{
		this->className = "Warrior";
	}
	else if (this->classType == 2)
	{
		this->className = "Assassin";
	}
	else if (this->classType == 3)
	{
		this->className = "Mage";
	}
}
void Unit::displayStats()
{
	cout << "HP: " << this->hp << endl;
	cout << "Pow: " << this->power << endl;
	cout << "Vit: " << this->vitality << endl;
	cout << "Agi: " << this->agility << endl;
	cout << "Dex: " << this->dexterity << endl;
}


bool Unit::hasAdvantage(Unit* opponent)//Checks which class has bonus damage 
{
	if (this->classType == 1 && opponent->classType == 2)
	{
		return true;
	}
	else if (this->classType == 2 && opponent->classType == 3)
	{
		return true;
	}
	else if (this->classType == 3 && opponent->classType == 1)
	{
		return true;
	}
	else
	{
		return false;
	}

}

bool Unit::canHit(Unit* opponent)//Checks if unit will evade attack
{
	srand(time(NULL));
	int evasionChance;
	int hitRate;
	hitRate = ((float)this->dexterity / (float)opponent->agility) * 100;
	evasionChance = rand() % 100 + 1;
	if (hitRate < 20)//Forces results to be within min 20 max 80
	{
		hitRate = 20;
	}
	else if (hitRate > 80)
	{
		hitRate = 80;
	}

	if (evasionChance >= hitRate)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool Unit::attackFirst(Unit* opponent)//Checks turn order
{
	if (this->agility >= opponent->agility)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Unit::attackTarget(Unit* opponent)//Battle 
{
	if (this->hasAdvantage(opponent))//Applies bonus if unit has advatage over opponent
	{
		this->bonusDmg = 1.5;
	}
	else
	{
		this->bonusDmg = 1;
	}
	if (this->hp > 0)//For stopping the fight if hp is 0
	{
		this->damage = (this->power - opponent->vitality)*this->bonusDmg;//dmg formula
		if (this->damage == 0 || this->damage < 0)
		{
			this->damage = 1;
		}
		if (this->canHit(opponent) == false)//Evade
		{
			this->damage = 0;
			cout << "Missed " << endl;
		}
		opponent->hp -= this->damage;
		cout << this->unitName << " dealt " << this->damage << " to " << opponent->unitName << endl;
	}
	else
	{
		this->canFight = false;
	}
}