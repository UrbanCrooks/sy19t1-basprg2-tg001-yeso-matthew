#include <iostream>
#include <string>

using namespace std;

void sortArray()
{
	int length;
	cin >> length;
	int numbers[1000], i, j, smallest, temp;

	for (int i = 0; i < length; i++)
	{
		cin >> numbers[i];
	}
	for (i = 0; i < length; i++)
	{
		smallest = i;
		for (j = i + 1; j < length; j++)
		{
			if (numbers[j] < numbers[smallest])
			{
				smallest = j;
			}

		}
		temp = numbers[smallest];
		numbers[smallest] = numbers[i];
		numbers[i] = temp;
	}
	for (int i = 0; i < length; i++)
	{
		cout << numbers[i] << " ";
	}
	cout << endl;
}

int main()
{
	sortArray();
	cout << endl;

	system("pause");
	return 0;

}