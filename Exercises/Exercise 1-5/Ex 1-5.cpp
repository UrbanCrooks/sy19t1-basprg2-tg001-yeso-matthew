#include <iostream>
#include <string>
#include <time.h>

using namespace std;

int number[10];

void randArray(int number[])
{
	srand(time(NULL));

	for (int i = 0; i < 10; i++)
	{
		number[i] = rand() % 101;
		cout << number[i] << " ";
	}

	cout << endl;
}

void biggestNum(int numbers[])
{
	int largest = numbers[0];

	for (int i = 0; i < 10; i++)
	{
		if (largest < numbers[i])
		{
			largest = numbers[i];
		}
	}
	cout << "Largest number in array: " << largest << endl;
}

int main()
{

	randArray(number);
	cout << endl;

	biggestNum(number);
	cout << endl;

	system("pause");
	return 0;

}