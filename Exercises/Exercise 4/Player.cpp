#include <iostream>
#include <string>

#include "Player.h"

using namespace std;

void Player::Accessory::applyStatus()
{
	// Apply status with the accessory equipped.
	// e.g. Sprint Shoes (Equip Haste).
}

void Player::Accessory::applyBonuses()
{
	// Apply bonus stats with the accessory equipped.
	// e.g. Touph Ring (Vitality +50 & Spirit +50).
}

void Player::displayActions()
{
	// Display the actions that the character can use in battle.
}

void Player::modifyActionList()
{
	// Change the actions that the character can use in battle.
}
