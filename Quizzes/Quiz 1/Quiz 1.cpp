#include <iostream>
#include <string>

using namespace std;

int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };

void suggest(int gold)
{
	cout << "This is in your price range: " << endl;

	for (int i = 0; i < 7; i++)
	{
		if (gold >= packages[i])
		{
			cout << "Package #" << i + 1 << ": " << packages[i] << endl;
		}
	}
}

void arrange()
{
	int i, j, smallest, temp;
	
	for (i = 0; i < 7; i++)
	{
		smallest = i;
		for (j = i + 1; j < 7; j++)
		{
			if (packages[j] < packages[smallest])
			{
				smallest = j;
			}

		}
		temp = packages[smallest];
		packages[smallest] = packages[i];
		packages[i] = temp;
	}
	for (int i = 0; i < 7; i++)
	{
		cout << "Package #" << i + 1 << ": "  << packages[i] << endl;
	}
	cout << endl;
}

int main()
{
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	int gold = 250;
	int input;
	string answer;

	while (true)
	{
		arrange();

		cout << "Current Gold: " << gold << endl;
		cout << "Input price of package item to buy: ";
		cin >> input;
		cout << endl;

		for (int i = 0; i < 7; i++)
		{
			if (input == packages[i])
			{
				if (gold >= packages[i])
				{
					cout << "Item Purchased! " << endl << endl;
					gold -= packages[i];
				}
				else
				{
					cout << "Not enough gold " << endl << endl;
					suggest(gold);
				}
				break;
			}
			if (i == 6)
			{
				cout << "Package does not exist! " << endl << endl;
			}

		}

		system("pause");
		system("CLS");

		cout << "Current Gold: " << gold << endl;
		cout << "Do you want to continue shopping? (Y) Yes / (N) No ";
		cin >> answer;
		cout << endl;

		if (answer == "Y" || answer == "y")
		{

		}
		else if (answer == "N" || answer == "n")
		{
			
			system("CLS");

			cout << "Thank you for shopping with us! " << endl;

			system("pause");

			break;
			
			
		}
	}
	return 0;

}