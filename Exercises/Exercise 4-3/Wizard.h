#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Spell.h"

using namespace std;

class Wizard
{
public:

	string name;
	int hp;
	int mp;
	Spell*spell = new Spell;
	bool canFight = true;


	void attackOponent(Wizard* caster, Wizard* enemy);
};
