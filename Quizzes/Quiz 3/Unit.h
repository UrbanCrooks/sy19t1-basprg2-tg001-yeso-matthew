#pragma once
#include<string>
using namespace std;
class Unit
{
public:
	Unit();
	~Unit();
	
	string unitName;
	int classType;
	string className;
	int maxHP;
	int hp;
	int power;
	int vitality;
	int agility;
	int dexterity;
	int damage;
	float bonusDmg;
	bool canFight;
	void attackTarget(Unit* opponent);
	void initializeClass();
	void displayStats();
	bool attackFirst(Unit* opponent);
	bool hasAdvantage(Unit* opponent);
	bool canHit(Unit* opponent);

};
