#include "Wizard.h"
#include <iostream>
#include <string>

using namespace std;

void Wizard::attackOponent(Wizard* caster, Wizard* enemy)
{
	cout << caster->name << " HP: " << caster->hp << " MP: " << caster->mp << endl;
	cout << enemy->name << " HP: " << enemy->hp << " MP: " << enemy->mp << endl;
	cout << "========================" << endl;
	caster->mp -= caster->spell->mpCost;
	enemy->hp -= caster->spell->dmg;
	cout << caster->name << " cast " << caster->spell->name << " which costs " << caster->spell->mpCost << endl;
	cout << caster->name << " dealt " << caster->spell->dmg << " to " << enemy->name << endl;
	cout << endl;
	cout << enemy->name << " has " << enemy->hp << " hp " << " left " << endl;
	cout << endl;

	//Sets the canFight to false if hp or mana is depleted
	if (enemy->hp <= 0 || enemy->mp <= 0)
	{
		enemy->canFight = false;
		cout << caster->name << " has won " << endl;
	}
	else if (caster->hp <= 0 || caster->mp <= 0)
	{
		caster->canFight = false;
		cout << enemy->name << " has won " << endl;
	}
}