#include <iostream>
#include <string>

using namespace std;

void printArray(string items[])
{
	for (int i = 0; i < 8; i++)
	{
		cout << items[i] << " ";
	}
	cout << endl;
}

int main()
{

	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };

	printArray(items);

	system("pause");
	return 0;

}