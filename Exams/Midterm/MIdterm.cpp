#include <iostream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

struct Player //Struct for both players
{
	vector<string> deck;
	int eardrumDistance = 30;
	string side;
	int choice;
	string card;
	int cash = 0;
	int wager = 0;
	string result;
};

void createDeck(Player &holder);
void printDeck(Player &player);
void switchDeck(int& roundNumber, Player &player, Player &enemy);
void chooseCard(Player &player, Player &enemy);
void cardMatchups(Player &player, Player &enemy);
void emptyDeck(Player &player, Player &enemy);
void payout(Player &player);
void evaluateEnding(Player &player);

int main()
{
	Player player;
	Player enemy;

	for (int roundNumber = 0; roundNumber < 12; roundNumber++)
	{
		if (player.eardrumDistance <= 0)
		{
			break;
		}
		cout << "Round " << roundNumber + 1 << endl;
		cout << "Current cash: " << player.cash << endl;
		cout << "Distance to eardrum: " << player.eardrumDistance << endl;
		switchDeck(roundNumber, player, enemy);
		do
		{
			cout << "Place your wager(in mm): ";
			cin >> player.wager;
		} while ((player.wager < 1) || (player.wager > player.eardrumDistance));
		createDeck(player);
		createDeck(enemy);
		do
		{
			printDeck(player);
			chooseCard(player, enemy);
			cardMatchups(player, enemy);
		} while (player.result == "Tie");

		emptyDeck(player, enemy);
		payout(player);
	}
	cout << "Game Over your current cash is: " << player.cash << endl;
	evaluateEnding(player);

	system("pause");
	return 0;
}

void createDeck(Player &holder)//Function to fill up both decks
{
	for (int i = 0; i < 5; i++)
	{
		if (i == 0)
		{
			if (holder.side == "Emperor")
			{
				holder.deck.push_back("Emperor");
			}
			else if (holder.side == "Slave")
			{
				holder.deck.push_back("Slave");
			}
		}
		else
		{
			holder.deck.push_back("Civilian");
		}
	}
}

void printDeck(Player &player)//Function to print Deck for player 
{
	for (int i = 0; i < player.deck.size(); i++)
	{
		cout << "[" << i + 1 << "] " << player.deck[i] << endl;
	}
}

void switchDeck(int& roundNumber, Player &player, Player &enemy)//Switches sides every 3 rounds
{
	if (roundNumber % 6 == 0)//For round 1-3 and 7-9
	{
		player.side = "Emperor";
		enemy.side = "Slave";

	}
	else if (roundNumber % 3 == 0)//For round 4-6 and 10-12
	{
		player.side = "Slave";
		enemy.side = "Emperor";
	}
	cout << "You are now playing the " << player.side << " side" << endl;
}

void chooseCard(Player &player, Player &enemy)//Function for choosing both player's card
{
	srand(time(NULL));
	enemy.choice = rand() % enemy.deck.size();

	do
	{
		cout << "Which card will you play?";
		cin >> player.choice;
	} while (player.choice > player.deck.size());
	player.card = player.deck[player.choice - 1];
	enemy.card = enemy.deck[enemy.choice];

	cout << player.card << endl;
	cout << enemy.card << endl;

}

void cardMatchups(Player &player, Player &enemy)//Function to compare cards
{
	if (player.card == enemy.card)//Draw
	{
		player.result = "Tie";
		cout << player.result << endl;
		player.deck.pop_back();
		enemy.deck.pop_back();
	}
	else if (player.card == "Emperor"&&enemy.card == "Civilian")//Player Emperor vs Enemy Citizen
	{
		player.result = "Win";
		cout << "Player " << player.result << "s" << endl;
	}
	else if (player.card == "Civilian"&&enemy.card == "Slave")//Player Citizen vs Enemy Slave
	{
		player.result = "Win";
		cout << "Player " << player.result << "s" << endl;
	}
	else if (player.card == "Slave"&&enemy.card == "Emperor")//Player Slave vs Enemy Emperor
	{
		player.result = "Win";
		cout << "Player " << player.result << "s" << endl;
	}
	else //Any other combination besides the ones above results in a loss
	{
		player.result = "Lose";
		cout << "Player " << player.result << "s" << endl;
	}
	system("pause");
	system("cls");
}

void emptyDeck(Player &player, Player &enemy)//Clears deck after every round
{
	player.deck.clear();
	enemy.deck.clear();
}

void payout(Player &player)// Function to determine player's reward/punishment
{
	if (player.result == "Win")
	{
		if (player.side == "Slave")//Reward is quitupled when in slave's side
		{
			cout << "You won " << player.wager * 500000 << " on the slave's side!!!" << endl;
			player.cash += (player.wager * 500000);
		}
		else
		{
			cout << "You won " << player.wager * 100000 << endl;
			player.cash += (player.wager * 100000);
		}
	}
	else if (player.result == "Lose")
	{
		player.eardrumDistance -= player.wager;
		cout << "Drill!!...Drill!!...Drill!!! " << endl;
		cout << "Drilled " << player.wager << endl;
	}
}

void evaluateEnding(Player &player)//Evaluate ending based on results
{
	if (player.eardrumDistance <= 0)
	{
		cout << "Bad Ending" << endl << endl;
	}
	else if (player.cash >= 20000000 && player.eardrumDistance < 30)
	{
		cout << "Best Ending" << endl << endl;
	}
	else if (player.cash <= 20000000 && player.eardrumDistance < 30)
	{
		cout << "Meh Ending" << endl << endl;
	}
}