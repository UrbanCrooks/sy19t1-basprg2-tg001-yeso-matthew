#include <iostream>
#include <time.h>

using namespace std;

void payout(int pRoll, int dRoll, int & gold, int &bet) 
{
	
	cout << "| RESULTS | " << endl << endl;
	
	if (pRoll == dRoll) // tie
	{
		cout << "It's a tie! Gold is returned." << endl << endl;
		gold += bet;
	}
	else if ((pRoll == 2) && (pRoll > dRoll)) 
	{
		cout << "You rolled Snake Eyes! You win the round and thrice your bet!" << endl << endl;
		gold += (bet * 3);
	}
	else if (pRoll > dRoll) 
	{
		cout << "You rolled higher than the dealer! " << endl << endl;
		gold += (bet * 2);
	}
	else 
	{ // player loses
		cout << "The Dealer rolled higher than you! " << endl << endl;
	}

	cout << "You have " << gold << " gold." << endl << endl;
}

void rollDice(int &diceA, int &diceB) {
	srand(time(NULL));
	diceA = rand() % 6 + 1;
	diceB = rand() % 6 + 1;

	cout << "Dice 1: " << diceA << endl;
	cout << "Dice 2: " << diceB << endl;
}

/*
int placeBet(int &gold) {
// bet function v1 (deduct player?s gold and return the bet as normal variable)

	int bet;
	do {
	// ask for bet
		cout << "You have " << gold << " gold." << endl;
		cout << "How much would you like to bet? ";
		cin >> bet;
		if (bet > gold) {
			cout << "You cannot bet more gold than you have." << endl;
		}
	} while (bet > gold);
	// if player bets more than they have, ask again

	gold -= bet;
	return bet;
}
*/

void placeBet(int &gold, int &bet) {
	// bet function (deduct players gold and bet will be set referencing)

	do {
		// ask for bet
		cout << "You have " << gold << " gold." << endl << endl;
		cout << "How much would you like to bet? ";
		cin >> bet;
		if (bet > gold) {
			cout << "You cannot bet more gold than you have." << endl << endl;
		}
	} while (bet > gold);
	// if player bets more than they have, ask again

	gold -= bet;
	cout << "You bet " << bet << " gold, and have " << gold << " gold remaining." << endl << endl;
}

void playRound(int &gold) {
	int bet = 0;
	int pDiceA, pDiceB, pRoll;
	int dDiceA, dDiceB, dRoll;

	cout << "| PLAYER'S TURN |" << endl << endl;
	// placeBet(gold); // use if using bet function v1
	placeBet(gold, bet);
	rollDice(pDiceA, pDiceB);
	pRoll = pDiceA + pDiceB;
	cout << "The player rolled a value of " << pRoll << "." << endl;

	cout << endl;
	system("pause");
	cout << endl;

	cout << "| Dealer's TURN |" << endl << endl;
	rollDice(dDiceA, dDiceB);
	dRoll = dDiceA + dDiceB;
	cout << "The Dealer rolled a roll value of " << dRoll << "." << endl;

	cout << endl;
	system("pause");
	cout << endl;

	payout(pRoll, dRoll, gold, bet);
	system("pause");
	system("CLS");
}

int main() {

	int gold = 1000;
	do {
		playRound(gold);
	} while (gold > 0);

	cout << "You have no more gold. Try again next time." << endl;
	system("pause");
	return 0;
}