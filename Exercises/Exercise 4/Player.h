#pragma once

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Player
{
	public:

		struct BasicInfo
		{
			string name;
			int level;
			int HP;
			int MP;
			int currentEXP;
			int nextEXP;
			int limitLevel;
		};

		struct MainStats
		{
			int strength;
			int dexterity;
			int vitality;
			int magic;
			int spirit;
			int luck;
		};

		struct BattleStats
		{
			int attack;
			int attackPercentage;
			int defense;
			int defensePercentage;
			int magicAttack;
			int magicDefense;
			int magicDefensePercentage;
		};

		struct Weapon
		{
			string weaponName;
			int attack;
			int attackPercentage;
			int defense;
			int defensePercentage;
			int magicAttack;
			int magicDefense;
			int magicDefensePercentage;
			int price;

			vector <string> materiaSlots;
			int materiaSlotsCount;
		};

		struct Armor
		{
			string armorName;
			int attack;
			int attackPercentage;
			int defense;
			int defensePercentage;
			int magicAttack;
			int magicDefense;
			int magicDefensePercentage;
			int price;

			vector <string> materiaSlots;
			int materiaSlotsCount;
		};

		struct Accessory
		{
			string accessoryName;
			int price;

			void applyStatus();
			void applyBonuses();
		};

		void displayActions();
		void modifyActionList();
};
