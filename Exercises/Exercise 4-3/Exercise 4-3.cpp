#include<iostream>
#include<string>
#include<vector>
#include"Spell.h"
#include"Wizard.h"

using namespace std;

int main()
{
	Wizard*firstWizard = new Wizard;
	Wizard*otherWizard = new Wizard;

	//Set 1st wizard's stats
	firstWizard->name = "Mew";
	firstWizard->hp = 100;
	firstWizard->mp = 200;
	firstWizard->spell->name = "Fire Ball";
	firstWizard->spell->dmg = 10;
	firstWizard->spell->mpCost = 20;

	//Sets 2nd wizard's stats
	otherWizard->name = "Kyo";
	otherWizard->hp = 80;
	otherWizard->mp = 200;
	otherWizard->spell->name = "Black Fire Ball";
	otherWizard->spell->dmg = 15;
	otherWizard->spell->mpCost = 30;

	do
	{
		firstWizard->attackOponent(firstWizard, otherWizard);
		otherWizard->attackOponent(otherWizard, firstWizard);
	} while (firstWizard->canFight == true && otherWizard->canFight == true);


	system("pause");
	return 0;
}