#include <iostream>
#include <string>
#include <time.h>
using namespace std;

void printMultipler(int& multipler) 
{
	cout << "Your multiplier is x" << multipler << "." << endl;
}

void printGold(int& gold) 
{
	cout << "You currently have " << gold << " gold." << endl;
}

void payout(int& gold, int rewards) 
{
	gold += rewards;
	printGold(gold);
}

struct Item
{
	string name;
	int gold;
};

Item *getRandomLoot(Item *looted) 
{
	// seed generator
	srand(time(NULL));

	// generate a random number between 1 and 5
	int random = rand() % 5 + 1;

	// put different values into the "name" and "value" of the object "looted" depending on the generated number
	switch (random) 
	{
	case 1:
		looted->name = "Mithril Ore";
		looted->gold = 100;
		break;
	case 2:
		looted->name = "Sharp Talon";
		looted->gold = 50;
		break;
	case 3:
		looted->name = "Thick Leather";
		looted->gold = 25;
		break;
	case 4:
		looted->name = "Jellopy";
		looted->gold = 5;
		break;
	case 5:
		looted->name = "Cursed Stone";
		looted->gold = 0;
		break;
	default:
		looted->name = "Cursed Stone";
		looted->gold = 0;
	}

	cout << "Looting..." << endl << endl;
	system("pause");
	system("CLS");

	cout << "You looted a " << looted->name;
	if (looted->name != "Cursed Stone") {
		cout << " worth " << looted->gold << " gold!" << endl;
	}
	else if (looted->name == "Cursed Stone") {
		cout << "! You died and lost all your gold! " << endl;
	}

	return looted;
}

int enterDungeon(int& gold, Item *looted) {
	int instances = 1, totalLootValue;
	string playerChoice;

	// pay 25 gold to enter
	gold -= 25;
	cout << "You pay 25 gold to enter the dungeon." << endl;
	printGold(gold);

	// loot function: run once, and then continue to loop as long as player did not loot a Cursed Stone 
	do {
		printMultipler(instances);
		cout << endl;
		getRandomLoot(looted);

		if (looted->name != "Cursed Stone") {
			// if the player has entered the dungeon more than once, display how much the item is worth times their dungeon multiplier
			if (instances != 1) {
				int product = looted->gold * instances;
				cout << "With your current multiplier, that item's worth " << product << " gold!" << endl << endl;
				totalLootValue += product;
				cout << "From all your items you have looted, you have a total of " << totalLootValue << " gold !" << endl << endl;
			}

			
			cout << "Would you like to continue looting? " << endl;
			cout << "	[Y]es" << endl;
			cout << "	[N]o" << endl;
			cin >> playerChoice;
			cout << endl;

			if (playerChoice == "Y" || playerChoice == "y") 
			{
				if (instances == 1) 
				{
					totalLootValue = looted->gold;
				}
				instances++;
			}
			else if (playerChoice == "N" || playerChoice == "n") 
			{
				break;
			}
		}
	} 
	
	while (looted->name != "Cursed Stone");

	return totalLootValue;
}

int main() {
	int gold = 50;
	Item *looted = new Item;


	cout << "Welcome to the Dungeon!" << endl << endl;
	cout << "You start with 50 gold." << endl << endl;
	
	system("pause");
	system("CLS");

	// enter dungeon function: run once, and then continue to loop as long as neither end game conditions are met
	do {
		int rewards = enterDungeon(gold, looted);
		if (looted->name != "Cursed Stone"); 
		{
			payout(gold, rewards);
		}
	} 

	while (gold < 25 || gold >= 500);
	
		if (gold < 25)
		{
			cout << "You do not have enough gold to enter the dungeon. Try again next time." << endl;
		}
		else if (gold >= 500)
		{
			cout << "Congratulations! You win! You reached the gold target of 500. " << endl;
		}
	
	delete looted;

	system("pause");
	return 0;
}