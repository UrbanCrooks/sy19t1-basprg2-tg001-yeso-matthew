#include <iostream>
#include <string>

using namespace std;

void check(string items[])
{
	int checker = 0;
	string input;

	cout << "What item would you like to see: ";
	getline(cin, input);

	for (int i = 0; i < 8; i++)
	{
		if (input == items[i])
		{
			cout << input << " exists" << endl;
			for (int j = 0; j < 8; j++)
			{
				if (input == items[j])
				{
					checker++;
				}
			}
			cout << "You have " << checker << " " << input << endl;
			break;
		}
		else if (i == 7)
		{
			cout << input << " does not exist" << endl;
		}
	}
}

int main()
{

	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };

	check(items);


	system("pause");
	return 0;

}