#include<iostream>
#include<string>
#include<vector>
#include<time.h>
#include "Unit.h"
using namespace std;

void createEnemy(int &roundNumber, Unit* enemy)//Creates an enemy with lower initial hp
{
	enemy->unitName = "Enemy ";
	enemy->classType = rand() % 3 + 1;
	enemy->initializeClass();
	enemy->hp = rand() % (roundNumber * 5) + (roundNumber * 5);
}

void afterBattle(Unit* player, Unit* opponent)//Adds stats and heals player if still alive
{
	if (player->hp > 0)
	{
		if (opponent->classType == 1)
		{
			cout << "You have gained +3 in HP and Vitality" << endl;
			cout << "Your new stats" << endl;
			player->maxHP += 3;
			player->vitality += 3;
		}
		else if (opponent->classType == 2)
		{
			cout << "You have gained +3 in Agility and Dexterity" << endl;
			cout << "Your new stats" << endl;
			player->agility += 3;
			player->dexterity += 3;
		}
		else if (opponent->classType == 3)
		{
			cout << "You have gained +5 in Power" << endl;
			cout << "Your new stats:" << endl;
			player->power += 5;
		}
		player->hp += (player->maxHP*.30);
		if (player->hp > player->maxHP)
		{
			player->hp = player->maxHP;
		}
	}
	else
	{
		system("cls");
		cout << "Game Over" << endl;
	}
}




int main()
{
	srand(time(NULL));
	Unit* player = new Unit;
	int roundNumber = 0;

	cout << "Enter name: ";
	cin >> player->unitName;
	cout << "Select class:" << endl;
	cout << "[1]Warrior" << endl;
	cout << "[2]Assassin" << endl;
	cout << "[3]Mage" << endl;
	cin >> player->classType;
	player->initializeClass();

	while (player->canFight == true)//player fights till dead
	{
		roundNumber++;
		cout << player->className << endl;
		Unit* enemy = new Unit;
		createEnemy(roundNumber, enemy);
		cout << "Round " << roundNumber << endl;

		player->displayStats();
		cout << "VS" << endl;
		cout << "Enemy " << enemy->className << endl;
		enemy->displayStats();
		system("pause");
		system("cls");
		while (player->canFight == true && enemy->canFight == true)//Fights till dead
		{
			if (player->attackFirst(enemy))
			{
				player->attackTarget(enemy);
				system("pause");
				enemy->attackTarget(player);
				system("pause");
			}
			else
			{
				enemy->attackTarget(player);
				system("pause");
				player->attackTarget(enemy);
				system("pause");
			}
		}
		system("cls");
		afterBattle(player, enemy);
		player->displayStats();
		system("pause");
		system("cls");
		delete enemy;
	}
	delete player;
	system("pause");
	return 0;
}